const Table = require('../models/tableModel');

function createTable (req, res) {
    const table = new Table();
    table.name = req.body.name;
    table.zona = req.body.zona;
    table.save((err, doc) => {
        if(err) {
            if(err.code == 11000) return res.status(422).send({errors: {name: {properties: {message: 'Nombre ya registrado'}}}});
            return res.status(422).send(err);
        }
        res.status(200).send(doc);
    })
}

async function getTables(req, res) {
    const tables = await Table.find();
    return res.status(200).send(tables);
}

function getOneTable(req, res) {
    const idTab = req.params.idTab;
    Table.findById(idTab, (err, value) => {
        if(err) return res.status(404).send({status: 'FAIL', message: 'Mesa no encontrada'});
        return res.status(200).send(value);
    });
}

function deleteTable(req, res) {
    Table.deleteOne({_id: req.body.id}, function(err) {
        if(err) {
            res.send(err);
        } else {
            res.send({data: 'Mesa Eliminada'});
        }
    });
}

module.exports = {
    createTable,
    getTables,
    getOneTable,
    deleteTable
}
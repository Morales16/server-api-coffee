const express = require('express');
const router = express.Router();

const { 
    newUser, 
    auth, 
    addCart, 
    updateCantCart,
    removeProdCart,
    getCartProducts
} = require('../controllers/userController');

const { 
    verifyToken, 
    isAdmin 
} = require('../middlewares/jwt');

router.post('/create', newUser);
router.post('/login', auth);

router.get('/cart-get', verifyToken, getCartProducts);

router.put('/cart-add/', verifyToken, addCart);
router.put('/cart-update', verifyToken, updateCantCart);

router.delete('/cart-remove/:idProd', verifyToken, removeProdCart)

module.exports = router;
const express = require('express');
const router = express.Router();

const {verifyToken, isAdmin} = require('../middlewares/jwt');
const {
    createTable,
    getTables,
    getOneTable,
    deleteTable
} = require('../controllers/tableController');

router.post('/create-table', isAdmin, createTable);

router.get('/', verifyToken, getTables);
router.get('/:idTab', verifyToken, getOneTable);

router.delete('/delete/:id', isAdmin, deleteTable);

module.exports = router;
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tableSchema = new Schema({
    name: {
        type: String,
        required: [true, 'El nombre de la mesa es requerida'],
        unique: true
    },
    zona: {
        type: String
    }
});

module.exports = mongoose.model('table', tableSchema);